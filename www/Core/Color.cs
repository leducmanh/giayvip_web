﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using Core.Basic;

namespace Core
{
    [Table("Color")]
    public partial class Color : PersistentEntity
    {
        public Color()
        {
            this.ProColors = new HashSet<ProColor>();
        }
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Img { get; set; }

        public virtual ICollection<ProColor> ProColors { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using Core.Basic;

namespace Core
{
    [Table("Bill_Customer")]
    public partial class Bill_Customer : PersistentEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int billid { get; set; }
        public Nullable<int> userid { get; set; }
        public string totalmoney { get; set; }
        public Nullable<System.DateTime> idate { get; set; }
        public Nullable<System.DateTime> xdate { get; set; }
        public string request { get; set; }
        public string typepay { get; set; }
        public Nullable<int> state { get; set; }
        public string lang { get; set; }
        public Nullable<int> show { get; set; }
        

        public virtual Customer Customer { get; set; }
        public virtual ICollection<BillDetail> BillDetails { get; set; }

        public Bill_Customer()
        {
            this.BillDetails = new HashSet<BillDetail>();
        }
    }
}

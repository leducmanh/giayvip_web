﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using Core.Basic;

namespace Core
{
     [Table("Customer")]
    public partial class Customer:PersistentEntity
    {
        public Customer()
        {
            this.Bill_Customer = new HashSet<Bill_Customer>();
        }
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]

        public int Id { get; set; }
        public string FullName { get; set; }
        public string Addr { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }

        public virtual ICollection<Bill_Customer> Bill_Customer { get; set; }
    }
}

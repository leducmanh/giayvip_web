﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using Core.Basic;

namespace Core
{
    [Table("ProSize")]
    public partial class ProSize:PersistentEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]

        public int Id { get; set; }
        public Nullable<int> ProId { get; set; }
        public Nullable<int> SizeId { get; set; }
 

        public virtual Product Product { get; set; }
        public virtual Size Size { get; set; }
    }
}

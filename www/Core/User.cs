﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using Core.Basic;

namespace Core
{
     [Table("User")]
    public partial class User:PersistentEntity
    {
         [Key]
         [DatabaseGenerated(DatabaseGeneratedOption.Identity)]

        public int Id { get; set; }
        public string FullName { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public Nullable<bool> Admin { get; set; }
        public Nullable<bool> Active { get; set; }
        public string Email { get; set; }
        public Nullable<int> Phone { get; set; }
        public string Avata { get; set; }
    }
}

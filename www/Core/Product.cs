﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using Core.Basic;


namespace Core
{
      [Table("Product")]
    public partial class Product : PersistentEntity
    {

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]

        public int Id { get; set; }
        public string Tag { get; set; }
        public string Name { get; set; }
        public string Content { get; set; }
        public string Detail { get; set; }
        public string Image { get; set; }
        public string Image1 { get; set; }
        public string Image2 { get; set; }
        public string Image3 { get; set; }
        public string Image4 { get; set; }
        public string Image5 { get; set; }
        public Nullable<double> Price { get; set; }
        public Nullable<double> PriceOld { get; set; }
        public Nullable<System.DateTime> Date { get; set; }
        public Nullable<int> CatId { get; set; }
        public string CatTag { get; set; }
        public Nullable<bool> Active { get; set; }
        public Nullable<int> Count { get; set; }
        public Nullable<System.DateTime> DateBegin { get; set; }
        public Nullable<System.DateTime> DateEnd { get; set; }

        public virtual ICollection<BillDetail> BillDetails { get; set; }
        public virtual Category Category { get; set; }
        public virtual ICollection<ProColor> ProColors { get; set; }
        public virtual ICollection<ProSize> ProSizes { get; set; }

        public Product()
        {
            this.BillDetails = new HashSet<BillDetail>();
            this.ProColors = new HashSet<ProColor>();
            this.ProSizes = new HashSet<ProSize>();
        }
    }
}

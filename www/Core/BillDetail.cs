﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using Core.Basic;

namespace Core
{
    [Table("BillDetail")]
    public partial class BillDetail:PersistentEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }
        public Nullable<int> bilid { get; set; }
        public Nullable<int> proid { get; set; }
        public Nullable<int> sizeid { get; set; }
        public Nullable<int> colorid { get; set; }
        public Nullable<int> quantity { get; set; }
        public string bilprice { get; set; }
        public string bilpricevnd { get; set; }
        public string bilmoney { get; set; }
        public Nullable<System.DateTime> Date { get; set; }
        public Nullable<int> status { get; set; }
        

        public virtual Bill_Customer Bill_Customer { get; set; }
        public virtual Product Product { get; set; }
    }
}

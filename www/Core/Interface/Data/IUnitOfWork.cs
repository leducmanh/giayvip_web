﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core.Interface.Data
{
    public interface IUnitOfWork
    {
        int Commit();
    }
}

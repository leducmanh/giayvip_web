﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Paging;
using System.Linq.Expressions;

namespace Core.Interface.Data
{
    public interface IRepository<T> where T : class
    {
        IQueryable<T> Entities { get; set; }

        IQueryable<T> GetAll();

        IQueryable<T> GetAllReadOnly();

        /// <summary>
        /// tra ve phan tu co id, null neu khong tim thay
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        T GetById(int id);

        T GetById(int? id);

        /// <summary>
        /// Tra ve phan tu thoa bieu thuc dieu kien, nem ra ngoai le neu nhieu hon 1 ket qua
        /// </summary>
        /// <param name="where"></param>
        /// <returns></returns>
        T Get(Expression<Func<T, bool>> where);

        /// <summary>
        /// Them moi mot phan tu
        /// </summary>
        /// <param name="entity"></param>
        void Add(T entity);

        /// <summary>
        /// cap nhat mot phan tu
        /// </summary>
        /// <param name="entity"></param>
        void Update(T entity);

        /// <summary>
        /// xoa mot phan tu bang cach dat co Deleted = 1
        /// </summary>
        /// <param name="entity"></param>
        void Delete(T entity);

        /// <summary>
        /// xoa nhieu phan tu thoa dieu kien bang cach dat co Deleted = 1
        /// </summary>
        /// <param name="where"></param>
        void Delete(Expression<Func<T, bool>> where);

        /// <summary>
        /// xoa vat ly mot phan tu 
        /// </summary>
        /// <param name="entity"></param>
        void DeletePersistent(T entity);

        /// <summary>
        /// xoa vat ly nhieu phan tu thoa dieu kien
        /// </summary>
        /// <param name="where"></param>
        void DeletePersistent(Expression<Func<T, bool>> where);

        /// <summary>
        /// trar ve toi da maxHints phan tu thoa dieu kien
        /// </summary>
        /// <param name="where"></param>
        /// <param name="maxHints"></param>
        /// <returns></returns>
        IQueryable<T> GetMany(Expression<Func<T, bool>> where, int? maxHints = null);

        /// <summary>
        /// Lay ve du lieu thuoc ve trang currnetPage voi so phan tu tren mot trang la pageSize va dieu kien tim kiem cho boi where, sap xep theo orderBy
        /// </summary>
        /// <typeparam name="TKey"></typeparam>
        /// <param name="where"></param>
        /// <param name="orderBy"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <param name="ascending"></param>
        /// <returns></returns>
        IPage<T> Page<TKey>(Expression<Func<T, bool>> where, Expression<Func<T, TKey>> orderBy, int currentPage, int pageSize, bool ascending = true);

        /// <summary>
        /// Lay ve du lieu thuoc ve trang currnetPage voi so phan tu tren mot trang la pageSize voi du lieu co san data, sap xep theo orderBy
        /// </summary>
        /// <typeparam name="TKey"></typeparam>
        /// <param name="data"></param>
        /// <param name="orderBy"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <param name="ascending"></param>
        /// <returns></returns>
        IPage<T> Page<TKey>(IQueryable<T> data, Expression<Func<T, TKey>> orderBy, int currentPage, int pageSize, bool ascending = true);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using Core.Basic;

namespace Core
{
     [Table("CommentProduc")]
    public partial class CommentProduc : PersistentEntity
    {
         [Key]
         [DatabaseGenerated(DatabaseGeneratedOption.Identity)]

        public int Id { get; set; }
        public Nullable<int> ProId { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public Nullable<int> Point { get; set; }
        public string Content { get; set; }
        public Nullable<System.DateTime> Date { get; set; }
        public Nullable<bool> Active { get; set; }
    }
}

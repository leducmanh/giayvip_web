﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core.Basic
{
    public abstract class PersistentEntity
    {
        public Nullable<int> Deleted { get; set; }
        public Nullable<System.DateTime> DeletedDate { get; set; }
    }
}

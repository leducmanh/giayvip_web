﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using Core.Basic;

namespace Core
{
       [Table("Size")]
    public partial class Size:PersistentEntity
    {
        public Size()
        {
            this.ProSizes = new HashSet<ProSize>();
        }
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]

        public int Id { get; set; }
        public string Name { get; set; }
        public Nullable<int> Deleted { get; set; }
        public Nullable<System.DateTime> DeletedDate { get; set; }

        public virtual ICollection<ProSize> ProSizes { get; set; }
    }
}

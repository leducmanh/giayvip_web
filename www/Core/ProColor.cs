﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using Core.Basic;

namespace Core
{
     [Table("ProColor")]
    public partial class ProColor :PersistentEntity
    {
         [Key]
         [DatabaseGenerated(DatabaseGeneratedOption.Identity)]

        public int Id { get; set; }
        public Nullable<int> ProId { get; set; }
        public Nullable<int> ColorId { get; set; }

        public virtual Color Color { get; set; }
        public virtual Product Product { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ClassLibrary.Validations
{
    public class PasswordValidation : ValidationAttribute
    {
        string username;

        public string Username
        {
            get { return username; }
            set { username = value; }
        }
        int maxLength, minLength;

        public PasswordValidation(string username, int maxLength, int minLength)
        {
            this.username = username;
            this.maxLength = maxLength;
            this.minLength = minLength;
        }

        public override bool IsValid(object value)
        {
            string val = value.ToString();

            if (val.Length < minLength)
            {
                base.ErrorMessage = "Tên đăng nhập có độ dài thấp nhất là " + minLength.ToString();
                return false;
            }

            if (val.Length > maxLength)
            {
                base.ErrorMessage = "Tên đăng nhập có độ dài tối đa là " + maxLength.ToString();
                return false;
            }

            if (username.Contains(val))
            {
                base.ErrorMessage = "Mật khẩu không được chưa tên đăng nhập ";
                return false;
            }
            return true;
        }
    }
}
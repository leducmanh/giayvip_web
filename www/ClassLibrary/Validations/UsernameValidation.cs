﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ClassLibrary.Validations
{
    public class UsernameValidation : StringLengthAttribute
    {
        public UsernameValidation(int maximumLength)
            : base(maximumLength)
        {
        }

        public override bool IsValid(object value)
        {
            string val = Convert.ToString(value);
            if (val.Length < base.MinimumLength)
            {
                base.ErrorMessage = "Tên đăng nhập có độ dài thấp nhất là " + base.MinimumLength.ToString();
                return false;
            }

            if (val.Length > base.MaximumLength)
            {
                base.ErrorMessage = "Tên đăng nhập có độ dài tối đa là " + base.MaximumLength.ToString();
                return false;
            }

            return true;
        }
    }
}
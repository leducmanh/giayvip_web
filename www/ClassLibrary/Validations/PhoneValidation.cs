﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ClassLibrary.Validations
{
    public class PhoneValidation : StringLengthAttribute
    {
        public PhoneValidation(int maximumLength)
            : base(maximumLength)
        {
        }

        public override bool IsValid(object value)
        {
            string val = value.ToString();

            char[] cc = val.ToCharArray();
            for (int i = 0; i < cc.Length; i++)
            {
                if (!(cc[i] >= '0' && cc[i] <= '9'))
                {
                    base.ErrorMessage = "Số điện thoại không được chứa kí tự ";
                    return false;
                }
            }
            if (val.Length < base.MinimumLength)
            {
                base.ErrorMessage = "Số điện thoại có độ dài thấp nhất là " + base.MinimumLength.ToString();
                return false;
            }

            if (val.Length > base.MaximumLength)
            {
                base.ErrorMessage = "Số điện thoại có độ dài tối đa là " + base.MaximumLength.ToString();
                return false;
            }

            return true;
        }
    }
}
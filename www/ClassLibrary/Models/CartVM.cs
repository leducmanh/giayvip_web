﻿using ClassLibrary.Models;
namespace ClassLibrary.Models
{
    public class CartVM
    {
        public Product Product { get; set; }
        public int Quantity { get; set; }
    }
}
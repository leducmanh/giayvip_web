﻿using ClassLibrary.Validations;
using System.ComponentModel.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;


namespace ClassLibrary.Models
{
    public static class Account
    {
        public static string name = "";
        public static bool admin = false;
        public static string capcha = "";
        public static int id = 0;
    }

    public class LoginModel
    {
        [Required]
        [Display(Name = "User name")]
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }
    }


    public class RegisterModel
    {
        [Required]
        [Display(Name = "Tên Đăng Nhập: ")]
        [UsernameValidation(20, MinimumLength = 6)]
        public string UserName { get; set; }


        [Required]
        [Display(Name = "Tên Đây Đủ: ")]
        [UsernameValidation(50, MinimumLength = 6)]
        public string FullName { get; set; }
     

        [Required]
        [PasswordValidation("asdsd", 20, 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Mật Khẩu: ")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Nhập Lại Mật Khẩu: ")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        [RegularExpression(@"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" +
         @"\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" +
         @".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$", ErrorMessage = "Email không đúng")]
        [Display(Name = "Email: ")]
        public string Email { get; set; }

        [Required]
        [PhoneValidation(11, MinimumLength = 6)]
        [Display(Name = "Điện Thoại Liên Lạc")]
        public int Phone { get; set; }
    }

    public class CaptchaImageResult : ActionResult
    {
        public string GetCaptchaString(int length)
        {
            int intZero = '0';
            int intNine = '9';
            int intA = 'A';
            int intZ = 'Z';
            int intCount = 0;
            int intRandomNumber = 0;
            string strCaptchaString = "";

            Random random = new Random(System.DateTime.Now.Millisecond);

            while (intCount < length)
            {
                intRandomNumber = random.Next(intZero, intZ);
                if (((intRandomNumber >= intZero) && (intRandomNumber <= intNine) || (intRandomNumber >= intA) && (intRandomNumber <= intZ)))
                {
                    strCaptchaString = strCaptchaString + (char)intRandomNumber;
                    intCount = intCount + 1;
                }
            }
            return strCaptchaString;
        }


        public override void ExecuteResult(ControllerContext context)
        {
            Bitmap bmp = new Bitmap(100, 30);
            Graphics g = Graphics.FromImage(bmp);
            g.Clear(System.Drawing.Color.Navy);
            string randomString = GetCaptchaString(6);
            context.HttpContext.Session["captchastring"] = randomString;

            g.DrawString(randomString, new Font("Courier", 16), new SolidBrush(System.Drawing.Color.WhiteSmoke), 2, 2);
            HttpResponseBase response = context.HttpContext.Response;
            response.ContentType = "image/jpeg";
            bmp.Save(response.OutputStream, ImageFormat.Jpeg);
            bmp.Dispose();
        }
    }
}

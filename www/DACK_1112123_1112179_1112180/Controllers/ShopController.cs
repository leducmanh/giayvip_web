﻿using ClassLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DACK_1112123_1112179_1112180.Controllers
{
    public class ShopController : Controller
    {
        // Hiển thị danh sách đơn hàng

        giay_vipEntities db = new giay_vipEntities();
        public ActionResult Index()
        {
            return View(db.Bill_Customer);
        }

        #region Create Order
        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Create([Bind(Exclude = "OrderID")]Bill_Customer model)
        {
            var CartList = (List<CartVM>)Session["Cart"];
            var OrdetailsList = new BillDetail();
            
            
            model.state = 0;
            var total = CartList.Sum(m => m.Product.Price * m.Quantity);
            model.totalmoney = total.ToString();
            model.idate = DateTime.Now;
            model.xdate = DateTime.Now;
            
            // thêm danh sách OrderDetails
            
            // Thêm bảng Order
            db.Bill_Customer.Add(model);
            db.SaveChanges();
            foreach (var pro in CartList)
            {
                OrdetailsList.proid = pro.Quantity;
                db.BillDetails.Add(OrdetailsList);
                db.SaveChanges();
            }
            OrdetailsList.bilid = model.billid;
            UpdateModel(OrdetailsList);
            Session["Cart"] = null;
            return RedirectToAction("CreateSuccess");
        }
        public ActionResult CreateSuccess()
        {
            return View();
        }
        #endregion

        #region Details Order
        public ActionResult Details(int id)
        {
            return View(db.Bill_Customer.Find(id));
        }
        #endregion
        #region Delete Order
        [HttpPost]
        public ActionResult Delete(int id)
        {
            var _Order = db.Bill_Customer.Find(id);
            _Order.BillDetails.ToList().ForEach(m => db.BillDetails.Remove(m));
            db.Bill_Customer.Remove(_Order);
            db.SaveChanges();
            return new EmptyResult();
        }
        #endregion

        #region ShoppingCart
        public ActionResult ShowCart()
        {
            return View();
        }
        public ActionResult AddToCart(int id)
        {
            var _Product = db.Products.Find(id);
            var CartList = new List<CartVM>();
            if (Session["Cart"] != null)
            {
                CartList = (List<CartVM>)Session["Cart"];
                var OldCart = CartList.Find(m => m.Product.Id == id);
                if (OldCart != null)
                {
                    var NewCart = new CartVM { Product = _Product, Quantity = OldCart.Quantity + 1 };
                    CartList.Remove(OldCart);
                    CartList.Add(NewCart);
                }
                else
                {
                    CartList.Add(new CartVM { Product = _Product, Quantity = 1 });
                }
            }
            else
            {
                CartList.Add(new CartVM { Product = _Product, Quantity = 1 });
            }
            Session["Cart"] = CartList;
            return RedirectToAction("ShowCart");
        }
        public ActionResult RemoveCart(int id)
        {
            var CartList = (List<CartVM>)Session["Cart"];
            var _Cart = CartList.SingleOrDefault(m => m.Product.Id == id);
            CartList.Remove(_Cart);
            Session["Cart"] = CartList;
            return new EmptyResult();
        }
        #endregion

    }
}

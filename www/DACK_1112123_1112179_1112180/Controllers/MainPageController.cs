﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using ClassLibrary.Models;
using System.Net.Mail;
using System.Net;

namespace DACK_1112123_1112179_1112180.Controllers
{
    public class MainPageController : Controller
    {
        giay_vipEntities db = new giay_vipEntities();


        public ActionResult Tim(string search)
        {
            if (string.IsNullOrEmpty(search)) return RedirectToAction("Index", "MainPage");
            else
            {

                ViewData.Model = db.Products.Where(m => m.Name.Contains(search)).ToList();

            }
            return View();
        }

        #region [Show product]
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Details(int id)
        {
            return View(db.Products.Find(id));
        }

        [HttpPost]
        public ActionResult Details(int id, FormCollection col)
        {
            var cmtpro = new CommentProduc();

            //cmtpro.Email = "leducmanh@live.com";
            cmtpro.Name = col["namecmt"];
            cmtpro.Content = col["contentcmt"];
            cmtpro.Date = DateTime.Now;
            cmtpro.Point = 10;
            cmtpro.ProId = id;

            db.CommentProducs.Add(cmtpro);
            db.SaveChanges();
            return View(db.Products.Find(id));
        }

        public ActionResult ProductCategory()
        {
            return View(db.Categories);
        }

        public ActionResult Product(int id)
        {
            return View(db.Products.Where(m => m.CatId == id));
        }

        #endregion
        #region [Account]

        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginModel u)
        {
            if (ModelState.IsValid)
            {
                using (db)
                {
                    User v = new User();
                  try
                  {
                      v = db.Users.Where(a => a.UserName.Equals(u.UserName) && a.Password.Equals(u.Password)).FirstOrDefault();
                      
                  }
                  catch (System.Exception ex)
                  {
                      ViewBag.ErrLogin = "Tên đăng nhập hoặc mật khẩu không đúng";
                      return View();
                  }

                  if (v.Active == false)
                  {
                      ViewBag.ErrLogin = "Tài khoản chưa được kích hoạt. Vui lòng vào kích hoạt qua email: " + v.Email;
                      return View();
                  }
                    
                  if (v != null)
                  {
                      Session["LogedUserID"] = v.UserName.ToString();
                      Session["LogedUserFullname"] = v.FullName.ToString();
                      if (v.Admin == true)
                          Account.admin = true;
                      Account.name = v.FullName;
                      Account.id = v.Id;
                      return RedirectToAction("Index");
                  }
                    
                }
            }
            return View();
        }

        public ActionResult Logout()
        {
            Account.name = "";
            return RedirectToAction("Index");
        }


        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Register(FormCollection col, User u)
        {

            User user = new User();

            string name = col["FullName"];
            string username = col["UserName"];
            string pass = col["Password"];
            string email = col["Email"];
            string phone = col["Phone"];

            user.FullName = name;
            user.UserName = username;
            user.Password = pass;
            user.Email = email;
            user.Phone = int.Parse(phone);
            user.Active = false;
            user.Admin = false;
           user.Avata = "temp.png";

            if (col["CaptchaText"] != HttpContext.Session["captchastring"].ToString())               
            {
                ViewBag.Message = "CAPTCHA verification failed!";
                return View();
            }

            User tmp = new User();
            try
            {
                tmp = db.Users.First(m => m.UserName == username);
            }
            catch (System.Exception ex)
            {
                tmp = null;
            }
            
            if (tmp != null)
            {
                ViewBag.ErrUser = "Tên người dùng đã tồn tại";
                return View();
            }
            user.ConfirmPassword = col["ConfirmPassword"];
            ClassLibrary.Models.Account.name = name;
            
            db.Users.Add(user);
            db.SaveChanges();
            SendVerificationMail(user);
            ViewBag.ConfMail = "Your account have been created. Please check your e-mail to activate it." + user.Email.ToString();
            return RedirectToAction("RegisterSucsess");


        }

        public ActionResult RegisterSucsess()
        {
            return View();
        }

        private void SendVerificationMail(User user)
        {

            MailMessage mail = new MailMessage();
            mail.Subject = "Your Subject";
            mail.From = new MailAddress("it.hutech.oanh@gmail.com");
            mail.To.Add(user.Email);
            mail.Body = "Activation link: http://localhost:2497/MainPage/EditAccount/" + user.Id.ToString();
            mail.IsBodyHtml = true;

            SmtpClient smtp = new SmtpClient("smtp.gmail.com", 587);
            smtp.EnableSsl = true;
            NetworkCredential netCre = new NetworkCredential("it.hutech.oanh@gmail.com", "030406089495");
            smtp.Credentials = netCre;

            try
            {
                smtp.Send(mail);
            }
            catch (Exception ex)
            {
            }
           
        }



        public CaptchaImageResult ShowCaptchaImage()
        {
            return new CaptchaImageResult();
        }

        public ActionResult AdminManager()
        {
            if (ClassLibrary.Models.Account.name.ToString() == "")
            {
                return RedirectToAction("Login");
            }
            return View();
        }

        public ActionResult AccountManager()
        {
            if (ClassLibrary.Models.Account.name.ToString() == "")
            {
                return RedirectToAction("Login");
            }
            return View(db.Users);
        }

        public ActionResult CreateAccount()
        {
            if (ClassLibrary.Models.Account.name.ToString() == "")
            {
                return RedirectToAction("Login");
            }
            return View();
        }

        [HttpPost]
        public ActionResult CreateAccount(FormCollection col, User u)
        {
            if (ClassLibrary.Models.Account.name.ToString() == "")
            {
                return RedirectToAction("Login");
            }
            User user = new User();
            string name = col["FullName"];
            string username = col["UserName"];
            string pass = col["Password"];
            string email = col["Email"];
            string phone = col["Phone"];

            user.FullName = name;
            user.UserName = username;
            user.Password = pass;
            user.Email = email;
            user.Phone = int.Parse(phone);
            user.Active = true;
            if (col["Admin"] == "Admin")
                user.Admin = true;
            else
                user.Admin = false;
            user.Avata = "";
            user.ConfirmPassword = col["ConfirmPassword"];
            db.Users.Add(user);
            db.SaveChanges();
            return RedirectToAction("AccountManager");
        }




        public ActionResult EditAccount(int id)
        {
            return View(db.Users.Find(id));
        }

        [HttpPost]
        public ActionResult EditAccount(FormCollection col, int id)
        {

            var u = db.Users.First(m => m.Id == id);
            db.Users.Remove(u);
            db.SaveChanges();
            User acc = new User();
            string name = col["FullName"];
            string username = col["UserName"];
            string pass = col["Password"];
            string email = col["Email"];
            string phone = col["Phone"];

            acc.FullName = name;
            acc.UserName = username;
            acc.Password = pass;
            acc.Email = email;
            acc.Phone = int.Parse(phone);
            acc.Active = true;

            if (col["Admin"] == "Admin")
                acc.Admin = true;
            else
                acc.Admin = false;
            acc.Avata = "";
            acc.ConfirmPassword = col["Password"];
            //UpdateModel(acc);
            db.Users.Add(acc);
            db.SaveChanges();
            return RedirectToAction("AccountManager");
        }


        public ActionResult DeleteAccount(int id)
        {
            var acc = db.Users.First(m => m.Id == id);
            db.Users.Remove(acc);
            db.SaveChanges();
            return RedirectToAction("AccountManager");
        }


        public ActionResult ProductManager()
        {
            if (ClassLibrary.Models.Account.name.ToString() == "")
            {
                return RedirectToAction("Login");
            }
            return View(db.Products);
        }
        #endregion

        #region[Product Manager]
        public ActionResult EditProduct(int id, HttpPostedFileBase file)
        {
            if (ClassLibrary.Models.Account.name.ToString() == "")
            {
                return RedirectToAction("Login");
            }
            return View(db.Products.Find(id));
        }

        [HttpPost]
        public ActionResult EditProduct(FormCollection col, int id, HttpPostedFileBase file, HttpPostedFileBase file1, HttpPostedFileBase file2,
            HttpPostedFileBase file3, HttpPostedFileBase file4, HttpPostedFileBase file5)
        {

            var product = db.Products.First(m => m.Id == id);
            string image = col["Image"];
            string image1 = col["Image1"];
            string image2 = col["Image2"];
            string image3 = col["Image3"];
            string image4 = col["Image4"];
            string image5 = col["Image5"];
            string name = col["Name"];
            string content = col["Content"];


            float priceProduct = 0;
            float priceOldPro = 0;
            if (col["Price"] != "")
                priceProduct = float.Parse(col["Price"]);
            if (col["PriceOld"] != "")
                priceOldPro = float.Parse(col["PriceOld"]);

            product.Name = name;



            #region[file]
            if (file != null)
            {
                string pic = System.IO.Path.GetFileName(file.FileName);
                string path = System.IO.Path.Combine(
                                       Server.MapPath("~/Content/images"), pic);
                // file is uploaded
                file.SaveAs(path);

                // save the image path path to the database or you can send image 
                // directly to database
                // in-case if you want to store byte[] ie. for DB
                using (MemoryStream ms = new MemoryStream())
                {
                    file.InputStream.CopyTo(ms);
                    byte[] array = ms.GetBuffer();
                }
                product.Image = "";
                product.Image = file.FileName.ToString();
            }
            if (file1 != null)
            {
                string pic = System.IO.Path.GetFileName(file1.FileName);
                string path = System.IO.Path.Combine(
                                       Server.MapPath("~/Content/images"), pic);
                // file is uploaded
                file1.SaveAs(path);

                // save the image path path to the database or you can send image 
                // directly to database
                // in-case if you want to store byte[] ie. for DB
                using (MemoryStream ms = new MemoryStream())
                {
                    file1.InputStream.CopyTo(ms);
                    byte[] array = ms.GetBuffer();
                }
                product.Image1 = file1.FileName;
            }
            if (file2 != null)
            {
                string pic = System.IO.Path.GetFileName(file2.FileName);
                string path = System.IO.Path.Combine(
                                       Server.MapPath("~/Content/images"), pic);
                // file is uploaded
                file2.SaveAs(path);

                // save the image path path to the database or you can send image 
                // directly to database
                // in-case if you want to store byte[] ie. for DB
                using (MemoryStream ms = new MemoryStream())
                {
                    file2.InputStream.CopyTo(ms);
                    byte[] array = ms.GetBuffer();
                }
                product.Image2 = file2.FileName;
            }
            if (file3 != null)
            {
                string pic = System.IO.Path.GetFileName(file3.FileName);
                string path = System.IO.Path.Combine(
                                       Server.MapPath("~/Content/images"), pic);
                // file is uploaded
                file3.SaveAs(path);

                // save the image path path to the database or you can send image 
                // directly to database
                // in-case if you want to store byte[] ie. for DB
                using (MemoryStream ms = new MemoryStream())
                {
                    file3.InputStream.CopyTo(ms);
                    byte[] array = ms.GetBuffer();
                }
                product.Image3 = file3.FileName;
            }
            if (file4 != null)
            {
                string pic = System.IO.Path.GetFileName(file4.FileName);
                string path = System.IO.Path.Combine(
                                       Server.MapPath("~/Content/images"), pic);
                // file is uploaded
                file4.SaveAs(path);

                // save the image path path to the database or you can send image 
                // directly to database
                // in-case if you want to store byte[] ie. for DB
                using (MemoryStream ms = new MemoryStream())
                {
                    file4.InputStream.CopyTo(ms);
                    byte[] array = ms.GetBuffer();
                }
                product.Image4 = file4.FileName;
            }
            if (file5 != null)
            {
                string pic = System.IO.Path.GetFileName(file5.FileName);
                string path = System.IO.Path.Combine(
                                       Server.MapPath("~/Content/images"), pic);
                // file is uploaded
                file5.SaveAs(path);

                // save the image path path to the database or you can send image 
                // directly to database
                // in-case if you want to store byte[] ie. for DB
                using (MemoryStream ms = new MemoryStream())
                {
                    file5.InputStream.CopyTo(ms);
                    byte[] array = ms.GetBuffer();
                }
                product.Image5 = file5.FileName;
            }
            #endregion
            product.Content = content;
            product.Price = priceProduct;
            product.PriceOld = priceOldPro;
            UpdateModel(product);
            db.SaveChanges();
            return RedirectToAction("ProductManager");
        }

        public ActionResult DeleteProduct(int id)
        {
            if (ClassLibrary.Models.Account.name.ToString() == "")
            {
                return RedirectToAction("Login");
            }
            var acc = db.Products.First(m => m.Id == id);
            db.Products.Remove(acc);
            db.SaveChanges();
            return RedirectToAction("ProductManager");
        }


        public ActionResult CreateProduct()
        {
            if (ClassLibrary.Models.Account.name.ToString() == "")
            {
                return RedirectToAction("Login");
            }
            return View();
        }

        [HttpPost]
        public ActionResult CreateProduct(FormCollection col, Product u, HttpPostedFileBase file, HttpPostedFileBase file1, HttpPostedFileBase file2,
            HttpPostedFileBase file3, HttpPostedFileBase file4, HttpPostedFileBase file5)
        {
            string nameproduct = col["name"];
            string content = col["content"];
            float priceProduct = 0;
            float priceOldPro = 0;
            if (col["price"] != "")
                priceProduct = float.Parse(col["price"]);
            if (col["priceOld"] != "")
                priceOldPro = float.Parse(col["priceOld"]);

            #region[file]
            if (file != null)
            {
                string pic = System.IO.Path.GetFileName(file.FileName);
                string path = System.IO.Path.Combine(
                                       Server.MapPath("~/Content/images"), pic);
                // file is uploaded
                file.SaveAs(path);

                // save the image path path to the database or you can send image 
                // directly to database
                // in-case if you want to store byte[] ie. for DB
                using (MemoryStream ms = new MemoryStream())
                {
                    file.InputStream.CopyTo(ms);
                    byte[] array = ms.GetBuffer();
                }
                u.Image = file.FileName;
            }
            if (file1 != null)
            {
                string pic = System.IO.Path.GetFileName(file1.FileName);
                string path = System.IO.Path.Combine(
                                       Server.MapPath("~/Content/images"), pic);
                // file is uploaded
                file1.SaveAs(path);

                // save the image path path to the database or you can send image 
                // directly to database
                // in-case if you want to store byte[] ie. for DB
                using (MemoryStream ms = new MemoryStream())
                {
                    file1.InputStream.CopyTo(ms);
                    byte[] array = ms.GetBuffer();
                }
                u.Image1 = file1.FileName;
            }
            if (file2 != null)
            {
                string pic = System.IO.Path.GetFileName(file2.FileName);
                string path = System.IO.Path.Combine(
                                       Server.MapPath("~/Content/images"), pic);
                // file is uploaded
                file2.SaveAs(path);

                // save the image path path to the database or you can send image 
                // directly to database
                // in-case if you want to store byte[] ie. for DB
                using (MemoryStream ms = new MemoryStream())
                {
                    file2.InputStream.CopyTo(ms);
                    byte[] array = ms.GetBuffer();
                }
                u.Image2 = file2.FileName;
            }
            if (file3 != null)
            {
                string pic = System.IO.Path.GetFileName(file3.FileName);
                string path = System.IO.Path.Combine(
                                       Server.MapPath("~/Content/images"), pic);
                // file is uploaded
                file3.SaveAs(path);

                // save the image path path to the database or you can send image 
                // directly to database
                // in-case if you want to store byte[] ie. for DB
                using (MemoryStream ms = new MemoryStream())
                {
                    file3.InputStream.CopyTo(ms);
                    byte[] array = ms.GetBuffer();
                }
                u.Image3 = file3.FileName;
            }
            if (file4 != null)
            {
                string pic = System.IO.Path.GetFileName(file4.FileName);
                string path = System.IO.Path.Combine(
                                       Server.MapPath("~/Content/images"), pic);
                // file is uploaded
                file4.SaveAs(path);

                // save the image path path to the database or you can send image 
                // directly to database
                // in-case if you want to store byte[] ie. for DB
                using (MemoryStream ms = new MemoryStream())
                {
                    file4.InputStream.CopyTo(ms);
                    byte[] array = ms.GetBuffer();
                }
                u.Image4 = file4.FileName;
            }
            if (file5 != null)
            {
                string pic = System.IO.Path.GetFileName(file5.FileName);
                string path = System.IO.Path.Combine(
                                       Server.MapPath("~/Content/images"), pic);
                // file is uploaded
                file5.SaveAs(path);

                // save the image path path to the database or you can send image 
                // directly to database
                // in-case if you want to store byte[] ie. for DB
                using (MemoryStream ms = new MemoryStream())
                {
                    file5.InputStream.CopyTo(ms);
                    byte[] array = ms.GetBuffer();
                }
                u.Image5 = file5.FileName;
            }
            #endregion


            u.Name = nameproduct;

            u.Content = content;


            u.Price = priceProduct;


            u.PriceOld = priceOldPro;


            db.Products.Add(u);
            db.SaveChanges();
            return RedirectToAction("ProductManager");
        }

       

        #endregion 

        public ActionResult BillsManager()
        {
            return View(db.Bill_Customer);
        }

        public ActionResult DetailBill(int id)
        {
            ViewBag.id = id;
            return View(db.BillDetails);
        }
    }
}

#region [index]
//public ActionResult Index()
//        {
//            string str = "";
//            string value = "";            
//            var product = (from p in db.Products  select p).Take(8).ToList();
//            for (int i = 0; i < product.Count; i++)
//            {
//                var info = System.Globalization.CultureInfo.GetCultureInfo("vi-VN");
//                value = String.Format(info, "{0:c}", product[i].Price);
//                str += "<div class=\"item\">";
//                  str += "<div class=\"item-img\">";
//                      str += "<img src=\"" + "/Content/images/" + product[i].Image + ".png" + "\"/>";
//                  str += "</div>";
//                  str += "<div class=\"name-product\" href=\"http://google.com\" target=\"_blank\"> ";
//                   str += "<a href=\"http://google.com\" target=\""+product[i].Name+"\">" + product[i].Name + "</a>"; 
//                 str += "</div>";
//                str += "<div class=\"price-product\"> " + value + "</div>";
//                str += "</div>";
//            }
//            ViewBag.view = str;
//            return View();
//        }
#endregion
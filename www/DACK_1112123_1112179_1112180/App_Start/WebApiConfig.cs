﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace DACK_1112123_1112179_1112180
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }
}

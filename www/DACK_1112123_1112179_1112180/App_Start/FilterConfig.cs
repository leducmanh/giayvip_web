﻿using System.Web;
using System.Web.Mvc;

namespace DACK_1112123_1112179_1112180
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Disposable
{
    public class Disposable : IDisposable
    {
        bool disposed = false;

        ~Disposable()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing)
            {
                // Free any other managed objects here. 
                //
            }

            // Free any unmanaged objects here. 
            //
            disposed = true;
            DisposeCore();
        }

        public virtual void DisposeCore()
        {
            throw new NotImplementedException();
        }
    }
}

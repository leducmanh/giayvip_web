﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Paging
{
    public class PageAjax<T> : IPage<T> where T : class
    {
        public int CurrentPage { get; set; }
        public int TotalPage { get; set; }
        public int PageSize { get; set; }
        public int TotalCount { get; set; }
        public IEnumerable<T> Entities { get; set; }

        public PageAjax()
        {

        }

        public PageAjax(int currentPage, int pageSize, int totalCount)
        {
            this.CurrentPage = currentPage;
            this.PageSize = pageSize;
            this.TotalCount = totalCount;
            PageRecaculator();
        }

        public PageAjax(int currentPage, int pageSize, int totalCount, IEnumerable<T> entities)
            : this(currentPage, pageSize, totalCount)
        {
            this.Entities = entities;
            PageRecaculator();
        }

        public void PageRecaculator()
        {
            TotalPage = TotalCount / PageSize;
            if (TotalPage * PageSize < TotalCount)
            {
                TotalPage++;
            }
            if (CurrentPage > TotalPage)
            {
                CurrentPage = TotalPage;
            }
        }
    }
}

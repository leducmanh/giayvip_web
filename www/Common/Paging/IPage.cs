﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Paging
{
    public interface IPage<T> where T : class
    {
        int CurrentPage { get; set; }
        int TotalPage { get; set; }
        int PageSize { get; set; }
        int TotalCount { get; set; }
        IEnumerable<T> Entities { get; set; }
    }
}
